const express = require('express');

const PORT = 8080;

const app = express();


app.use(express.static('public'));

app.get('/', function (req, res) {
	var pokemons = require('./public/pokemon.json');
	const pokemonImages = require('./public/pokemonImages.json');

	pokemons.pokemon = pokemons.pokemon.map(function(obj){ 
   		obj.image = pokemonImages[obj.name];
   		return obj;
	});

	res.json(pokemons);
});

app.listen(PORT);
console.log('Running on http://localhost:' + PORT);